
/*==========  Imports  ==========*/


var gulp     	 = require('gulp'),
	sass         = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'), 
	minfy        = require('gulp-minify-css'), 	
	uglify       = require('gulp-uglify'),
	rename       = require('gulp-rename'),
	concat       = require('gulp-concat'),
	notify       = require('gulp-notify'),
	sourcemaps   = require('gulp-sourcemaps'),
	stylish      = require('jshint-stylish'),
	plumber      = require('gulp-plumber'),
	jshint       = require('gulp-jshint'),
	seq          = require('run-sequence'),
	clean        = require('del'), 
	imagemin     = require('gulp-imagemin'), 
	browsersync  = require('browser-sync'); 	     	 
 


/*==========  Methods  ==========*/


var onError = function(error) {
	notify.onError({
		title    : 'Gulp',
		subtitle : 'Failure!',
		message  : 'Error: <%= error.message %>',
		sound    : 'Beep'
	})(error);

	this.emit('end');
};


/*==========  Config  ==========*/


var config = {
	'css' : {
		'dist' : 'dist/css', 
		'src'  : 'sass/**/*.scss'
	}, 
	'js' : {
		'vendor': [
			'scripts/vendor/**/*.js'
		],			
		'dist'   : 'dist/js', 
		'src'    : 'scripts/main.js', 
		'id'     : 'main'
	},
	'browser': {
		'host': 'template.dev'
	}, 
	'images' : {
		'dist' : 'dist/img',
		'src'  : 'assets/images/*'
	}
}


/*==========  CSS  ==========*/


gulp.task('sass', function() { 
	return gulp.src(config.css.src)
		.pipe(plumber({errorHandler: onError}))
		.pipe(sass())
		.pipe(autoprefixer({
    		browsers: [
    			'last 2 versions', 
    			'safari 5', 
    			'ie 8', 
    			'ie 9', 
    			'opera 12.1', 
    			'ios 6', 
    			'android 4'
    		]
		}))
		.pipe(gulp.dest('./'))
		.pipe(minfy())
		.pipe(rename({suffix: '.min'}))
   		.pipe(gulp.dest(config.css.dist))
}); 


/*==========  JS  ==========*/


gulp.task('js-clean', function() {
	return clean(config.js.dist + '/*.js');
});

gulp.task('js-lint', function() {
	return gulp.src(config.js.src)		
		.pipe(jshint())
		.pipe(jshint.reporter(stylish));
});


gulp.task('js-scripts', ['js-lint'], function() {
	return gulp.src(config.js.vendor.concat(config.js.src))
		.pipe(plumber({errorHandler: onError}))
		.pipe(sourcemaps.init())
		.pipe(concat(config.js.id + '.js'))
		.pipe(uglify())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(config.js.dist));
});


/*==========  IMAGES  ==========*/


gulp.task('images', function() {
	gulp.src(config.images.src)
		.pipe(plumber({errorHandler: onError}))
		.pipe(imagemin())
		.pipe(gulp.dest(config.images.dist));
});


/*==========  BROWSERSYNC  ==========*/


gulp.task('browser-sync', function() {
	browsersync({
		proxy : config.browser.host,
		open  : false
	});
});

gulp.task('bs-reload', function() {
	browsersync.reload({once: true});
});

gulp.task('bs-css', function() {
	return gulp.src(config.css.id + '.css')
		.pipe(plumber({errorHandler: onError}))
		.pipe(browsersync.reload({stream: true}));
});


/*==========  WATCH tasks  ==========*/


gulp.task('watch', function () {	

  	gulp.watch([config.js.src, config.js.vendor], function() {
		seq('js-scripts', 'bs-reload');
	});

  	gulp.watch(config.css.src, function() {
		seq('sass', 'bs-reload');
	});

});	

gulp.task('default', function() { 
	gulp.start(['watch', 'browser-sync', 'images']);    
}); 